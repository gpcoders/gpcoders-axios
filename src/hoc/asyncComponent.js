import React, { Component } from 'react'

/**
 * @param importComponent
 * @returns {{new(): {state, componentDidMount(): void, render(): *}}}
 */
const gpAsyncComponent = (importComponent) => {
    return class extends Component {
        state = {
            gpComponent: null
        }

        /**
         * @ { Lifecycle hook }
         */
        componentDidMount () {
            importComponent()
                .then(gpCmp => {
                    this.setState({gpComponent: gpCmp.default})
                })
        }

        render() {
            const AsyncedComponent = this.state.gpComponent

            return AsyncedComponent ? <AsyncedComponent {...this.props} /> : null
        }
    }
}

export default gpAsyncComponent