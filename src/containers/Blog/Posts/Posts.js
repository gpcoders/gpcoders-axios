import React, {Component} from 'react';
import { Link } from 'react-router-dom'
import axios from '../../../axios';
import Post from '../../../components/Post/Post'
import './Post.css'


class Posts extends Component {
    state = {
        posts: [],
        loader: false
    }

    componentDidMount () {
        console.log(this.props)
        this.setState({ loader: true })
        axios.get( '/posts' )
            .then( response => {
                const posts = response.data.slice(0, 4);
                const updatedPosts = posts.map(post => {
                    return {
                        ...post,
                        author: 'Max'
                    }
                });
                this.setState({posts: updatedPosts, loader: false});
                // console.log( response );
            } )
            .catch(error => {
                 console.log(error);
                //this.setState({error: true});
            });
    }

    postSelectedHandler = (id) => {
        console.log(id)
        this.props.history.push({pathname: '/posts/' + id})
    }


    render() {
        let posts = <p style={{textAlign: 'center'}}>Something went wrong!</p>;
        if (this.state.loader) {
            return (
                <p style={{textAlign: 'center'}}>Loading...</p>
            )
        }
        if (!this.state.error) {
            posts = this.state.posts.map(post => {
                return <Link to={"/" + post.id} key={post.id}>
                    <Post
                        key={post.id}
                        title={post.title}
                        author={post.author}
                        clicked={() => this.postSelectedHandler(post.id)}
                     />
                 </Link>;
            });
        }
        return (
            <div>
                <section className="Posts">
                    {posts}
                </section>
            </div>

        );
    }
}

export default Posts;
