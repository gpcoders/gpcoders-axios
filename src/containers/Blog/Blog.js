import React, { Component } from 'react';
import { Route, NavLink, Switch } from 'react-router-dom'

import './Blog.css';
import Posts from './Posts/Posts'
import gpAsyncComponent from '../../hoc/asyncComponent'
import FullPost from "./FullPost/FullPost";

/**
 * @ { Async or lazy loading }
 */
const AsyncNewPost = gpAsyncComponent(() => {
    return import('./NewPost/NewPost');
});

class Blog extends Component {
    state = {
        selectedPostId: null,
        error: false
    }



    render () {

        return (
            <div className="Blog">
                <header>
                    <nav>
                        <ul>
                            <li><NavLink
                                to="/posts"
                                exact
                                activeClassName="my-active"
                                activeStyle={{
                                    color: 'red',
                                    textDecoration: 'undreline'
                                }}
                            >Posts</NavLink></li>
                            <li><NavLink to={{
                                pathname: '/new-post',
                                hash: '#gugu',
                                search: '?quick-submit=true'
                            }}>New Post</NavLink></li>
                        </ul>
                    </nav>
                </header>

                <Switch>
                    <Route path="/posts" component={Posts} />
                    <Route path="/new-post" component={AsyncNewPost} />
                    <Route path='/:id' exact component={FullPost} />
                    <Route render={() => <h1>Not Found</h1>} />
                </Switch>
            </div>
        );
    }
}

export default Blog;